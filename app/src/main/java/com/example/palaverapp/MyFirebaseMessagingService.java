package com.example.palaverapp;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    public static final String CHANNEL_ID = "Channel 12";


    @Override
    public void onNewToken(String token) {

        SharedPreference sp = new SharedPreference();
        String username = sp.getLoggedUser(this, SharedPreference.KEY_USERNAME);
        if(!username.equals(SharedPreference.DEFAULT_STR)){
            sendTokenToServer(token);
        }

    }

    public void sendTokenToServer(String token) {
        SharedPreference sp = new SharedPreference();
        String url = HttpHandler.baseURLString+"/api/user/pushtoken ";
        String username = sp.getLoggedUser(this, SharedPreference.KEY_USERNAME);
        String password = sp.getLoggedUser(this, SharedPreference.KEY_PASSWORD);
        String hashedpass = new Hashing().encode(password);
        HttpHandler httpHandler = new HttpHandler(this);
        String jsonStr = "{\"Username\":\""+username+"\",\"Password\":\""+hashedpass+"\", " +
                "\"PushToken\":\""+token+"\"}";
        RequestAsyncTask requestAsyncTask = new RequestAsyncTask();
        requestAsyncTask.execute(url, jsonStr, httpHandler);
        String response = null;
        try {
            response = requestAsyncTask.get();
            if (!response.equals(null)) {
                //JSONObject jsonObjectResponse = new JSONObject(response);
                JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                if (jsonObjectResponse.getInt("MsgType") == 1) {
                    //Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                    Log.d(TAG, "Token successfully sent");
                }
                else{
                    //MsgType is 0
                    Log.d(TAG, "Token not successfully sent");
                }
            }
            else{
                Log.d(TAG, "ERROR.. no Server response");
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Map<String, String> map = remoteMessage.getData();
            //HashMap<String, String > data = new HashMap<String, String>(map);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                CharSequence name = getString(R.string.channel_name);
                String description = getString(R.string.channel_description);
                int importance = NotificationManager.IMPORTANCE_DEFAULT;
                NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
                channel.setDescription(description);
                // Register the channel with the system; you can't change the importance
                // or other notification behaviors after this
                NotificationManager notificationManager = getSystemService(NotificationManager.class);
                notificationManager.createNotificationChannel(channel);
            }
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.putExtra("friend_key", map.get("sender"));
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, intent, PendingIntent.FLAG_ONE_SHOT);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "channel_id")
                    .setContentTitle(map.get("sender"))
                    .setContentText(map.get("preview"))
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setStyle(new NotificationCompat.BigTextStyle())
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setSmallIcon(R.mipmap.app_icon)
                    .setAutoCancel(true);

            notificationBuilder.setContentIntent(pendingIntent);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(getResources().getInteger(R.integer.notificationID), notificationBuilder.build());
        }


    }
}

