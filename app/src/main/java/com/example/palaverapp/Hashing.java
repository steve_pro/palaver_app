package com.example.palaverapp;

public class Hashing {

    private static  final  int THRESHOLD = 100;
    private static final int key = 37;

    public String encode(String plaintext){
        String hashedPassword = this.hashFunction(plaintext);
        return hashedPassword;

    }

    private String hashFunction(String string){
        StringBuilder sb=new StringBuilder();
        for(int i=0;i<string.length();i++){
            int m=this.getAscii(string.charAt(i));
            sb.append(this.powerModulo(m, key, THRESHOLD));
        }
        return sb.toString();
    }

    private String modulo(int a, int n){
        int remainder=a%n;
        while(remainder<0){
            remainder+=n;
        }
        return Integer.toString(remainder);
    }

    private int getAscii(char c){
        int ascii=(int) c;
        return ascii;
    }

    private int powerModulo(int x,int e,int n){
        int count=0;
        int result=1;
        while(count<e){
            result*=x;
            result=Integer.parseInt(this.modulo(result, n));
            count++;
        }
        return result;
    }

}
