package com.example.palaverapp;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ContactListAdapter extends ArrayAdapter<String> {

    ArrayList<String> contacts;
    Context context;
    int id_itemcontact;

    public ContactListAdapter(Context context, int resource, ArrayList<String> contacts) {
        super(context, resource, contacts);
        this.contacts = contacts;
        this.context = context;
        id_itemcontact = resource;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public String getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View contactItemView = convertView;//prevents constant inflations, hence better performance
        final ContactItemViewHolder contactItemViewHolder;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if(contactItemView == null){
            contactItemViewHolder = new ContactItemViewHolder();
            contactItemView = inflater.inflate(id_itemcontact, parent, false);
            contactItemViewHolder.contactTextview = (TextView) contactItemView.findViewById(R.id.tv_contact);
            contactItemViewHolder.deleteContactButton  = (Button) contactItemView.findViewById(R.id.btn_deleteContact);
            contactItemView.setTag(contactItemViewHolder);
        }
        else{
            contactItemViewHolder   = (ContactItemViewHolder) contactItemView.getTag();
        }
        contactItemViewHolder.contactTextview.setText(getItem(position));
        final ContactListAdapter adapter = this;
        contactItemViewHolder.deleteContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeActivity homeActivity = (HomeActivity) context;
                KontactFragment kontactFragment1 = (KontactFragment) homeActivity.getSupportFragmentManager().findFragmentByTag(KontactFragment.TAG);
                //KontactFragment kontactFragment = KontactFragment.getInstance();
                String name = contactItemViewHolder.contactTextview.getText().toString().trim();
                if(kontactFragment1.deleteContact(name, context)){
                    contacts.remove(getItem(position));
                    adapter.notifyDataSetChanged();
                }
            }
        });
        return contactItemView;
    }

    static class ContactItemViewHolder{
        Button deleteContactButton, locationButton;
        TextView contactTextview;
    }

}
