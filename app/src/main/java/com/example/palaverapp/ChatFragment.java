package com.example.palaverapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ChatFragment extends Fragment {

    ArrayList<String> chat_list = new ArrayList<String>();
    ArrayAdapter<String> adapter;
    Intent intent;
    ListView listView;

    public static final String TAG = ChatFragment.class.getSimpleName();

    public static ChatFragment getInstance() {

        return new ChatFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container,false);
        listView = (ListView) view.findViewById(R.id.lv_chats);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        HomeActivity homeActivity = (HomeActivity) getActivity();
        KontactFragment kontactFragment = homeActivity.kontactFragment;
        ArrayList<String> list = kontactFragment.getContactListOffline(homeActivity,SharedPreference.KEY_CHAT_LIST);
        kontactFragment.addListToContactList(chat_list, list);
        adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, chat_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                            @Override
                                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                                String friend_name = chat_list.get(position);
                                                intent = new Intent(getActivity(), MainActivity.class);
                                                intent.putExtra("friend_key", friend_name);
                                                intent.putExtra("fragment", ChatFragment.TAG);
                                                startActivity(intent);
                                            }
                                        }
        );
    }
}
