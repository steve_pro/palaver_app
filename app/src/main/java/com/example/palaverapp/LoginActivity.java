
package com.example.palaverapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

public class LoginActivity extends BaseActivity {

    final int REGISTER_CODE = 1;
    static final String sp_login = "login_sp";
    final String DEFAULT_STR = "NULL";
    static final String KEY_USERNAME = "Username";
    public static final String TAG = LoginActivity.class.getSimpleName();
    public Activity activity;

    EditText username, password;
    Button loginBtn, registerBTN;
    TextView errTextview;
    HttpHandler httpHandler;
    SharedPreference sharedPref ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        sharedPref = new SharedPreference();
        String loggedUser = sharedPref.getLoggedUser(activity, SharedPreference.KEY_USERNAME);
        setContentView(R.layout.activity_login);
        username = (EditText) findViewById(R.id.et_usernameLogin);
        password = (EditText) findViewById(R.id.et_passwordLogin);
        errTextview = (TextView) findViewById(R.id.tv_errorLogin);
        registerBTN = (Button) findViewById(R.id.btn_toRegister);
        loginBtn = (Button) findViewById(R.id.btn_login);
        httpHandler = new HttpHandler(LoginActivity.this);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
        registerBTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toRegister();
            }
        });
        if(!(this.checkConnectivity())){
            Toast.makeText(LoginActivity.this, "Oops, no internet connection", Toast.LENGTH_LONG).show();
        }
    }

    private void login(){
        String usernameStr = username.getText().toString().trim();
        String passwordStr = password.getText().toString();
        if(!(usernameStr.equals("")) && !(passwordStr.equals(""))){
            Hashing hashing = new Hashing();
            try {
                if(this.checkConnectivity()){
                    String hashedpassword = hashing.encode(passwordStr);
                    JSONObject jsonObject = httpHandler.createJSON(usernameStr, hashedpassword);
                    RequestAsyncTask requestAsyncTask = new RequestAsyncTask(LoginActivity.this);
                    requestAsyncTask.execute(HttpHandler.baseURLString + "/api/user/validate", jsonObject.toString(), httpHandler);
                    String response = requestAsyncTask.get();
                    if (!response.equals(null)) {
                        JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                        if (jsonObjectResponse.getInt("MsgType") == 1) {
                            sharedPref.setLoggedUser(activity, SharedPreference.KEY_USERNAME, usernameStr);
                            sharedPref.setLoggedUser(activity, SharedPreference.KEY_PASSWORD, passwordStr);
                            Toast.makeText(LoginActivity.this, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(LoginActivity.this, com.example.palaverapp.HomeActivity.class);
                            startActivity(intent);
                        }
                        else{
                            Toast.makeText(LoginActivity.this, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        Toast.makeText(LoginActivity.this, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                    }
                }
                else{

                    final AlertDialog.Builder ad_builder = new AlertDialog.Builder(this);
                    ad_builder.setCancelable(false);
                    ad_builder.setTitle("Connectivity");
                    ad_builder.setMessage(R.string.res_no_internet);
                    ad_builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    AlertDialog alertDialog = ad_builder.create();
                    alertDialog.show();
                }

            }
            catch (Exception exc) {
                Log.d(TAG, exc.getMessage());
            }
        }
        else{
            String str = getString(R.string.res_registerERROR);
            errTextview.setText(str);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REGISTER_CODE){
            if(resultCode == RESULT_OK){
                errTextview.setText("");
                password.setText("");
                username.setText(data.getStringExtra("usernameKey"));
            }
            if(resultCode == RESULT_CANCELED){
                errTextview.setText("");
                password.setText("");
                username.setText("");
            }
        }
    }

    private void toRegister(){
        Intent intent = new Intent(LoginActivity.this, com.example.palaverapp.RegisterActivity.class );
        startActivityForResult(intent, REGISTER_CODE);

    }
}
