package com.example.palaverapp;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

public class RequestAsyncTask extends AsyncTask<Object, Void, String> {
    //to avoid memory leaks when an activity finishes before the background task
    private WeakReference<BaseActivity> weakReference;

    public RequestAsyncTask(BaseActivity activity){
        weakReference = new WeakReference<BaseActivity>(activity);
    }

    public RequestAsyncTask(){

    }

    @Override
    protected String doInBackground(Object... params) {
        String url = (String) params[0];
        String jsonStr = (String) params[1];
        HttpHandler httpHandler = (HttpHandler) params[2];
        String result = httpHandler.postData(url, jsonStr, "Content-Type","application/json");
        return result;
    }

}
