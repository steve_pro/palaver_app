package com.example.palaverapp;

public class Message {
   private String message;
   private boolean isMine;
   private int msg_type;
   static final int TYPE_SENT = 1;
   static final int TYPE_RECEIVED = 2;
    static final int TYPE_SENT_LOC = 3;
    static final int TYPE_RECEIVED_LOC = 4;

    public Message(String message, boolean isMine) {
        this.message = message;
        this.isMine = isMine;
        if(isMine){
            msg_type = Message.TYPE_SENT;
        }
        else{
            msg_type = Message.TYPE_RECEIVED;
        }
    }
    public Message(String message, boolean isMine, boolean isLoc){
        this.message = message;
        this.isMine = isMine;
        if(isMine){
            msg_type = Message.TYPE_SENT_LOC;
        }
        else{
            msg_type = Message.TYPE_RECEIVED_LOC;
        }
    }

    public String getMessage() {
        return message;
    }

    public int getMsg_type(){

        return msg_type;
    }

}
