package com.example.palaverapp;

import android.content.Context;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class HttpHandler {

    private static final String TAG = HttpHandler.class.getSimpleName();
    private Context context;
    public static final  String baseURLString = "http://palaver.se.paluno.uni-due.de";

    public HttpHandler(Context context){
        this.context = context;
    }

    public String postData(String urlString, String data, String user_agent_key, String user_agent_value){
        HttpURLConnection myConnection = null;
        OutputStream outputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter bufferedWriter = null;
        StringBuilder sb = null;
        BufferedReader bufferedReader = null;
        try {
            if(URLUtil.isHttpUrl(urlString) || URLUtil.isHttpsUrl(urlString)){
                URL url = new URL(urlString);
                myConnection = (HttpURLConnection) url.openConnection();
                myConnection.setRequestMethod("POST");
                myConnection.setDoOutput(true);
                if(!user_agent_key.equals(null) && !user_agent_value.equals(null)){
                    myConnection.setRequestProperty(user_agent_key,user_agent_value);
                }
                outputStream = myConnection.getOutputStream();
                outputStreamWriter = new OutputStreamWriter(outputStream);
                bufferedWriter = new BufferedWriter(outputStreamWriter);
                bufferedWriter.write(data);
                bufferedWriter.flush();
                bufferedWriter.close();
                myConnection.connect();

                InputStream is = myConnection.getInputStream();
                bufferedReader = new BufferedReader(new InputStreamReader(is));
                sb = new StringBuilder();
                String content = "";
                String newLine = System.getProperty("line.separator");
                while ((content = bufferedReader.readLine()) != null) {
                    sb.append(content + newLine);
                }
                bufferedReader.close();
            }
            else{
                Toast.makeText(context, "URL is not valid", Toast.LENGTH_LONG).show();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            myConnection.disconnect();
        }
        return sb.toString();
    }

    public JSONObject createJSON(String username, String password){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("Username",username);
            jsonObject.put("Password", password);
        } catch (JSONException exc) {
            Log.e(this.getClass().toString(),exc.getMessage());
        }
        return jsonObject;
    }

    public JSONObject stringToJSON(String jsonString){
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  jsonObject;
    }

}
