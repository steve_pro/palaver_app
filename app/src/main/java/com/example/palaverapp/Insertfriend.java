package com.example.palaverapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Insertfriend extends DialogFragment {

    static final String TAG = Insertfriend.class.getSimpleName();
    private EditText textinput;
    private TextView ok,cancel;

    public static Insertfriend getInstance(){
        return new Insertfriend();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.insertfriend, null);
        cancel = view.findViewById(R.id.action_cancel);
        ok = view.findViewById(R.id.action_ok);
        textinput = view.findViewById(R.id.input);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });


        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String input = textinput.getText().toString();
                   if(input.equals("")){
                       Toast.makeText(getContext(), "Enter a name", Toast.LENGTH_SHORT).show();
                       return;
                    }

                KontactFragment kontactFragment = (KontactFragment) getTargetFragment();
                if(kontactFragment == null){
                    return;
                }
                Intent intent = KontactFragment.newIntent(input);
                kontactFragment.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                getDialog().dismiss();
            }
        });

        return view;
    }

}