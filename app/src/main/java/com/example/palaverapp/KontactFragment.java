package com.example.palaverapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ExecutionException;


public class KontactFragment extends Fragment {

    private HomeActivity homeActivity;
    private ArrayList<String> arrayList_contacts = new ArrayList<String>();
    private ListView liste ;
    ArrayAdapter<String> adapter;
    private FloatingActionButton add_contact ;
    EditText friendname;
    public static final int INSERT_FRIEND_CODE = 1;
    public static final int MAIN_ACTIVITY_CODE = 2;
    public static final String TAG = KontactFragment.class.getSimpleName();
    public String fried_name;
    public static final String KEY_ADDED_FRIEND = "new friend";


    public  static KontactFragment getInstance(){
        KontactFragment kontactFragment = new KontactFragment();
        return kontactFragment;
    }


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.contactfragment, container, false);

        liste = (ListView) view.findViewById(R.id.listcontact);
        add_contact =  (FloatingActionButton) view.findViewById(R.id.addContact);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String urlRequest = "http://palaver.se.paluno.uni-due.de/api/friends/get";
        homeActivity = (HomeActivity) getActivity();
        adapter = (ContactListAdapter) new ContactListAdapter(homeActivity, R.layout.itemcontact ,arrayList_contacts);
        liste.setAdapter(adapter);
        add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Insertfriend insertfriend = Insertfriend.getInstance();
                insertfriend.setTargetFragment(KontactFragment.this, INSERT_FRIEND_CODE);
                insertfriend.show(getFragmentManager().beginTransaction(), insertfriend.TAG);
            }
        });
        if(homeActivity.checkConnectivity()){
            homeActivity.sharedPreference.setLoggedUser(homeActivity,SharedPreference.KEY_CONTACT_LIST,null);
            storeContactsLocally(getContactListOnline(), SharedPreference.KEY_CONTACT_LIST);
        }
        ArrayList<String> list = getContactListOffline(homeActivity, SharedPreference.KEY_CONTACT_LIST);
        addListToContactList(arrayList_contacts, list);
        liste.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String friendName = "";
                friendName = (String) ((ContactListAdapter)liste.getAdapter()).getItem(position);
                Intent intent = new Intent(homeActivity, MainActivity.class);
                intent.putExtra("friend_key", friendName);
                intent.putExtra("fragment", KontactFragment.TAG);
                startActivityForResult(intent, MAIN_ACTIVITY_CODE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode != Activity.RESULT_OK){
            return;
        }
        if(requestCode == INSERT_FRIEND_CODE){
            String name = data.getStringExtra(KEY_ADDED_FRIEND);
            if(sendContactToServer(name)){
                storeContactsLocally(getContactListOnline(), SharedPreference.KEY_CONTACT_LIST);
                ArrayList<String> friends_names = getContactListOffline(homeActivity, SharedPreference.KEY_CONTACT_LIST);
                addListToContactList(arrayList_contacts, friends_names);
                homeActivity.getSupportFragmentManager().beginTransaction().detach(this).attach(this).commit();
            }
        }
        if(requestCode == MAIN_ACTIVITY_CODE){
            String name = data.getStringExtra("friend_key");
            saveToChat(name);
        }
    }

    private boolean sendContactToServer(String name){
        boolean successfullySent = false;
        SharedPreference sp = homeActivity.sharedPreference;
        if(!homeActivity.checkConnectivity()){
            Toast.makeText(homeActivity, R.string.res_no_internet, Toast.LENGTH_SHORT).show();
        }
        else{
            String url = HttpHandler.baseURLString+"/api/friends/add ";
            String username = sp.getLoggedUser(homeActivity, SharedPreference.KEY_USERNAME);
            String password = sp.getLoggedUser(homeActivity, SharedPreference.KEY_PASSWORD);
            String hashedpass = new Hashing().encode(password);
            String friend = name;
            HttpHandler httpHandler = homeActivity.httpHandler;
            String jsonStr = "{\"Username\":\""+username+"\",\"Password\":\""+hashedpass+"\", " +
                    "\"Friend\":\""+friend+"\"}";
            RequestAsyncTask requestAsyncTask = new RequestAsyncTask(homeActivity);
            requestAsyncTask.execute(url, jsonStr, httpHandler);
            //Checking the result of the POST request
            //String response = this.getServerResponse();
            String response = null;
            try {
                response = requestAsyncTask.get();
                if (!response.equals(null)) {
                    JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                    if (jsonObjectResponse.getInt("MsgType") == 1) {
                        Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                        successfullySent = true;
                    }
                    else{
                        Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Toast.makeText(homeActivity, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return successfullySent;
    }


    public void storeContactsLocally(ArrayList<String> names, String key){
        ArrayList<String> contacts = names;
        if(contacts.size() == 0){
            return;
        }
        else{
            StringBuilder sb = new StringBuilder();
            for(String str : contacts){
                sb.append(str);
                sb.append(",");
            }
            homeActivity.sharedPreference.setLoggedUser(homeActivity,key ,sb.toString());
        }
    }

    public void saveToChat(String name){
        ArrayList<String> chats = getContactListOffline(homeActivity, SharedPreference.KEY_CHAT_LIST);
        ArrayList<String> list = arrayList_contacts;
        chats.add(name);
        if(chats.size() > 0) {
                for (int j = 0; j < chats.size(); j++) {
                    if(!list.contains(chats.get(j))){
                        chats.remove(j);
                    }

                }
        }
        chats = new ArrayList<>(new HashSet<>(chats));
        storeContactsLocally(chats, SharedPreference.KEY_CHAT_LIST);
    }

    public ArrayList<String> getContactListOffline(Context context, String key){
        HomeActivity homeActivity = (HomeActivity) context;
        String list = homeActivity.sharedPreference.getLoggedUser(homeActivity, key);
        ArrayList<String> contacts = new ArrayList<String>();
        if (!list.equals(SharedPreference.DEFAULT_STR)) {
            String[] names = list.split(",");
            for(int i = 0;i<names.length;i++){
                contacts.add(names[i]);
            }
        }
        return contacts;
    }


    private ArrayList<String> getContactListOnline(){
        ArrayList<String> contacts = new ArrayList<>();
        SharedPreference sp = homeActivity.sharedPreference;
        if(!homeActivity.checkConnectivity()){
            Toast.makeText(homeActivity, R.string.res_no_internet, Toast.LENGTH_SHORT).show();
        }
        else{
            String url = HttpHandler.baseURLString+"/api/friends/get ";
            String username = sp.getLoggedUser(homeActivity, SharedPreference.KEY_USERNAME);
            String password = sp.getLoggedUser(homeActivity, SharedPreference.KEY_PASSWORD);
            String hashedpass = new Hashing().encode(password);
            HttpHandler httpHandler = homeActivity.httpHandler;
            String jsonStr = "{\"Username\":\""+username+"\",\"Password\":\""+hashedpass+"\"}";
            RequestAsyncTask requestAsyncTask = new RequestAsyncTask(homeActivity);
            requestAsyncTask.execute(url, jsonStr, httpHandler);
            //Checking the result of the POST request
            //String response = this.getServerResponse();
            String response = null;
            try {
                response = requestAsyncTask.get();
                if (!response.equals(null)) {
                    //JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                    if (jsonObjectResponse.getInt("MsgType") == 1) {
                        //contact added successfully
                        //Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                        final JSONArray jsonArray = jsonObjectResponse.getJSONArray("Data");
                        if(jsonArray.length() > 0){
                            String name = "";
                            for(int i = 0;i<jsonArray.length();i++){
                                name = jsonArray.getString(i);
                                contacts.add(name);
                            }
                        }
                        else {
                            Toast.makeText(homeActivity, "No contacts saved", Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Toast.makeText(homeActivity, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        return contacts;
    }

    public boolean deleteContact(String name, Context context){
        boolean successfullyDeleted = false;
        homeActivity = (HomeActivity) context;
        if(!homeActivity.checkConnectivity()){
            Toast.makeText(homeActivity, "Connect to Internet", Toast.LENGTH_SHORT).show();
        }
        else{
            String url = HttpHandler.baseURLString+"/api/friends/remove";
            String username = homeActivity.sharedPreference.getLoggedUser(homeActivity, SharedPreference.KEY_USERNAME);
            String password = homeActivity.sharedPreference.getLoggedUser(homeActivity, SharedPreference.KEY_PASSWORD);
            String hashedPassword = new Hashing().encode(password);
            String jsonStr = "{\"Username\":\""+username+"\",\"Password\":\""+hashedPassword+"\", " +
                    "\"Friend\":\""+name+"\"}";
            RequestAsyncTask requestAsyncTask = new RequestAsyncTask(homeActivity);
            requestAsyncTask.execute(url, jsonStr, homeActivity.httpHandler);
            String response = null;
            try {
                response = requestAsyncTask.get();
                if (!response.equals(null)) {
                    //JSONObject jsonObjectResponse = new JSONObject(response);
                    JSONObject jsonObjectResponse = homeActivity.httpHandler.stringToJSON(response);
                    if (jsonObjectResponse.getInt("MsgType") == 1) {
                        //contact added successfully
                        Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                        successfullyDeleted = true;
                        final String str = name;
                        final KontactFragment kontactFragment = this;
                        homeActivity.runOnUiThread(new Runnable(){
                            public void run() {

                                homeActivity.sharedPreference.removeValueSharedPreference(homeActivity, SharedPreference.KEY_CONTACT_LIST);
                                storeContactsLocally(getContactListOnline(), SharedPreference.KEY_CONTACT_LIST);
                                ArrayList<String> arrayList = getContactListOffline(homeActivity,SharedPreference.KEY_CONTACT_LIST);
                                addListToContactList(arrayList_contacts, arrayList);


                            }
                        });
                    }
                    else{
                        Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Toast.makeText(homeActivity, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(successfullyDeleted){
        }
        return successfullyDeleted;
    }
    public static Intent newIntent(String name){
        Intent intent = new Intent();
        intent.putExtra(KEY_ADDED_FRIEND, name);
        return intent;
    }

    public void addListToContactList(ArrayList<String> arrayList_contacts, ArrayList<String> arrayList) {
        if(!arrayList.equals(null)){
            if(arrayList.size() > 0) {
                for (int i = 0; i < arrayList_contacts.size(); i++) {
                    for (int j = 0; j < arrayList.size(); j++) {
                        if (arrayList_contacts.get(i).equals(arrayList.get(j))) {
                            arrayList.remove(j);
                        }

                    }
                }
            }
            arrayList_contacts.addAll(arrayList);
        }

    }

}
