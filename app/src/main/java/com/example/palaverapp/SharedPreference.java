package com.example.palaverapp;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

    public static final String SP_LOGIN = "loginSharedPref";
    public static final String KEY_USERNAME = "Username";
    public static final String KEY_PASSWORD = "Password";
    public static final String KEY_CONTACT_LIST = "Contacts";
    public static final String KEY_CHAT_LIST = "Chats";
    public static final String DEFAULT_STR = "NULL";

    public SharedPreference(){

    }

    public void setLoggedUser(Context context, String key, String value) {
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        sharedPreferences = context.getSharedPreferences(SP_LOGIN, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getLoggedUser(Context context, String key) {
        SharedPreferences sharedPreferences;
        sharedPreferences = context.getSharedPreferences(SP_LOGIN, Context.MODE_PRIVATE);

        return sharedPreferences.getString(key, DEFAULT_STR);
    }

    public void removeValueSharedPreference(Context context, String key){
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;
        sharedPreferences = context.getSharedPreferences(SP_LOGIN, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.remove(key).commit();
    }

}
