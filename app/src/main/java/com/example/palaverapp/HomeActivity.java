package com.example.palaverapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class HomeActivity extends BaseActivity {

    Toolbar mToolbar;
    KontactFragment kontactFragment;
    ChatFragment chatFragment;
    SettingFragment settingFragment;
    public SharedPreference sharedPreference;
    public HttpHandler httpHandler;
    public static HomeActivity homeActivity;
    public static final String TAG = HomeActivity.class.getSimpleName();
    public static String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null){
            setContentView(R.layout.activity_home);
            homeActivity = this;
            sharedPreference = new SharedPreference();
            httpHandler = new HttpHandler(this);
            setToken();
            mToolbar = (Toolbar) findViewById(R.id.toolbar_home);
            mToolbar.setTitle(sharedPreference.getLoggedUser(homeActivity, SharedPreference.KEY_USERNAME));
            BottomNavigationView bottomnav =findViewById(R.id.bottomNavView_Bar);
            bottomnav.setOnNavigationItemSelectedListener(navListener);
            kontactFragment = KontactFragment.getInstance();
            chatFragment = ChatFragment.getInstance();
            settingFragment = SettingFragment.getInstance();
            getSupportFragmentManager().beginTransaction().add(R.id.container, settingFragment, SettingFragment.TAG).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, chatFragment, ChatFragment.TAG).commit();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, kontactFragment, KontactFragment.TAG).commit();
        }
    }

    public void sendTokenToServer(String token) {
        if(!homeActivity.checkConnectivity()){
            Toast.makeText(homeActivity, "Oops.. no internet", Toast.LENGTH_SHORT);
            return;
        }
        SharedPreference sp = new SharedPreference();
        String url = HttpHandler.baseURLString+"/api/user/pushtoken ";
        String username = sp.getLoggedUser(homeActivity, SharedPreference.KEY_USERNAME);
        String password = sp.getLoggedUser(homeActivity, SharedPreference.KEY_PASSWORD);
        String hashedpass = new Hashing().encode(password);
        HttpHandler httpHandler = new HttpHandler(homeActivity);
        String jsonStr = "{\"Username\":\""+username+"\",\"Password\":\""+hashedpass+"\", " +
                "\"PushToken\":\""+token+"\"}";
        RequestAsyncTask requestAsyncTask = new RequestAsyncTask();
        requestAsyncTask.execute(url, jsonStr, httpHandler);
        String response = null;
        try {
            response = requestAsyncTask.get();
            if (!response.equals(null)) {
                JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                if (jsonObjectResponse.getInt("MsgType") == 1) {
                    Log.d(TAG, "Token successfully sent");
                }
                else{

                    Log.d(TAG, "Token not successfully sent");
                }
            }
            else{
                Log.d(TAG, "ERROR.. no Server response");
            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setToken(){
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "Token retrieval failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        token = task.getResult().getToken();
                        sendTokenToServer(token);
                        // Log and toast
                        String msg = "FCM token : "+ token;
                        Log.d(TAG, msg);
                    }
                });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            Fragment clickedFragment = null;
            String tag = "";
        switch (menuItem.getItemId()){
        case R.id.Bottonbarcontact:
        clickedFragment = kontactFragment;
        tag = KontactFragment.TAG;
        break;
        case R.id.Bottonbarchat:
        clickedFragment = chatFragment;
        tag = ChatFragment.TAG;
        break;
        case R.id.Bottonbarsetting:
        clickedFragment = settingFragment;
        tag = SettingFragment.TAG;
         break;
            }
            if(!clickedFragment.equals(null)){
  getSupportFragmentManager().beginTransaction().replace(R.id.container ,clickedFragment, tag).addToBackStack(null).commit();}
        return true;
        }
    };

    }
