package com.example.palaverapp;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

public class MessageListAdapter extends RecyclerView.Adapter {


    private Context activity;
    private ArrayList<Message> messageList;

    public MessageListAdapter(Context context, ArrayList<Message> list){
        activity = context;
        messageList = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        switch (i){
            case(Message.TYPE_RECEIVED):
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.received_message, viewGroup, false);
                return  new ReceivedViewHolder(view);
            case (Message.TYPE_SENT):
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sent_message, viewGroup, false);
                return  new SentViewHolder(view);
            case (Message.TYPE_RECEIVED_LOC):
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.received_message_geo, viewGroup, false);
                return new ReceivedLocViewHolder(view);
            case (Message.TYPE_SENT_LOC):
                view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sent_message_geo, viewGroup, false);
                return  new SentLocViewHolder(view);
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        int view_type = viewHolder.getItemViewType();
        Message msg = messageList.get(i);
        System.out.println(view_type);
        switch(view_type) {
            case (Message.TYPE_SENT):
                SentViewHolder sntHolder = (SentViewHolder) viewHolder;
                sntHolder.bind(msg);
                break;

            case (Message.TYPE_RECEIVED):
                ReceivedViewHolder rcvHolder = (ReceivedViewHolder) viewHolder;
                rcvHolder.bind(messageList.get(i));
                break;
            case (Message.TYPE_SENT_LOC):
                SentLocViewHolder snt_loc_holder = (SentLocViewHolder) viewHolder;
                snt_loc_holder.bind(messageList.get(i));
                break;
            case (Message.TYPE_RECEIVED_LOC):
                ReceivedLocViewHolder rcv_loc_holder = (ReceivedLocViewHolder) viewHolder;
                rcv_loc_holder.bind(messageList.get(i));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        Message msg =  messageList.get(position);
        return msg.getMsg_type();
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    private  class  SentViewHolder extends RecyclerView.ViewHolder{
        TextView msg_sent;
        public SentViewHolder(@NonNull View itemView) {
            super(itemView);
            msg_sent = (TextView) itemView.findViewById(R.id.tv_sentMsg);
        }

        private void bind(Message message){
            msg_sent.setText(message.getMessage());
        }
    }

    private class ReceivedViewHolder extends RecyclerView.ViewHolder{
        TextView msg_received;

        public ReceivedViewHolder(@NonNull View itemView) {
            super(itemView);
            msg_received = (TextView) itemView.findViewById(R.id.tv_receivedMsg);
        }
            private void bind(Message message){
                msg_received.setText(message.getMessage());
            }
    }
    private  class  SentLocViewHolder extends RecyclerView.ViewHolder{
        TextView msg_sent;
        public SentLocViewHolder(@NonNull View itemView) {
            super(itemView);
            msg_sent = (TextView) itemView.findViewById(R.id.tv_location_sent);
        }

        private void bind(Message message){
            final String [] coordinates = message.getMessage().split(",");
            msg_sent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //TODO show location
                    double lat = Double.parseDouble(coordinates[0]);
                    double lon = Double.parseDouble(coordinates[1]);
                    Intent intent = new Intent(activity, MapsActivity.class);
                    intent.putExtra("latitude", lat);
                    intent.putExtra("longitude", lon);
                    activity.startActivity(intent);
                    Log.d("MessageListAdapter test", msg_sent.getText().toString());
                }
            });
        }
    }

    private class ReceivedLocViewHolder extends RecyclerView.ViewHolder{
        TextView msg_received;

        public ReceivedLocViewHolder(@NonNull View itemView) {
            super(itemView);
            msg_received = (TextView) itemView.findViewById(R.id.tv_location_received);
        }
        private void bind(Message message){
            final String [] coordinates = message.getMessage().split(",");
            msg_received.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    double lat = Double.parseDouble(coordinates[0]);
                    double lon = Double.parseDouble(coordinates[1]);
                    Intent intent = new Intent(activity, MapsActivity.class);
                    intent.putExtra("latitude", lat);
                    intent.putExtra("longitude", lon);
                    activity.startActivity(intent);
                    Log.d("MessageListAdapter test", msg_received.getText().toString());
                }
            });
        }
    }
}
