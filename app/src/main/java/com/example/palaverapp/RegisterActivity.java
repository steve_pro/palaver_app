package com.example.palaverapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.security.SecureRandom;
import java.util.Random;

public class RegisterActivity extends BaseActivity {

    EditText username, password, repeatPassword;
    Button btn_register, btn_cancel;
    TextView errorRegister;
    HttpHandler httpHandler;


    private static final Random RANDOM = new SecureRandom();
    private static final String ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        username = (EditText) findViewById(R.id.et_username);
        password = (EditText) findViewById(R.id.et_password);
        repeatPassword = (EditText) findViewById(R.id.et_repeatPassword);
        errorRegister = (TextView) findViewById(R.id.tv_errorRegister);
        btn_register = (Button) findViewById(R.id.btn_register);
        btn_cancel = (Button) findViewById(R.id.btn_cancel);
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerUser();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelRegistration();
            }
        });
        httpHandler = new HttpHandler(RegisterActivity.this);
        if(!(this.checkConnectivity())){
            Toast.makeText(RegisterActivity.this,R.string.res_no_internet, Toast.LENGTH_LONG).show();
        }
    }

    private void registerUser(){
        String usernameStr, passwordStr, rptPasswordStr, salt, hashedPassword;
        usernameStr = username.getText().toString().trim();
        passwordStr = password.getText().toString().trim();
        rptPasswordStr = repeatPassword.getText().toString().trim();
        //TODO pressed register button and toast of oops no internet came. must fix
        if(!(usernameStr.equals("")) && !(passwordStr.equals("")) && !(rptPasswordStr.equals(""))) {
            if (passwordStr.equals(rptPasswordStr)) {
                Hashing hashing = new Hashing();
                try {
                    if(this.checkConnectivity()){
                        String userSecret = hashing.encode(passwordStr);
                        //HttpHandler httpHandler = new HttpHandler(RegisterActivity.this);
                        JSONObject jsonObject = httpHandler.createJSON(usernameStr, userSecret);
                        RequestAsyncTask requestAsyncTask= new RequestAsyncTask(RegisterActivity.this);
                        String url = HttpHandler.baseURLString+"/api/user/register";
                        requestAsyncTask.execute(url, jsonObject.toString(), httpHandler);
                        //Checking the result of the POST request
                        //String response = this.getServerResponse();
                        String response = requestAsyncTask.get();
                        if(!response.equals(null)){
                            JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                            if(jsonObjectResponse.getInt("MsgType") == 1){
                                Toast.makeText(RegisterActivity.this, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                                Intent intent = getIntent();
                                intent.putExtra("usernameKey", usernameStr);
                                setResult(RESULT_OK, intent);
                                RegisterActivity.this.finish();
                            }
                            else{
                                //user already exists
                                errorRegister.setText("Username already exists");

                            }
                        }
                        else{
                            //ERROR... no respose from server
                            Toast.makeText(RegisterActivity.this, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                        }
                    }
                    else{
                        final AlertDialog.Builder ad_builder = new AlertDialog.Builder(this);
                        ad_builder.setCancelable(false);
                        ad_builder.setTitle("Connectivity");
                        ad_builder.setMessage(R.string.res_no_internet);
                        ad_builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                        AlertDialog alertDialog = ad_builder.create();
                        alertDialog.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                //passwords do not match
                errorRegister.setText("Passwords do not match");
            }
        }
        else{
            String str = getString(R.string.res_registerERROR);
            errorRegister.setText(str);
        }

    }

    private void cancelRegistration(){
        //takes user back to login activity
        setResult(RESULT_CANCELED);
        RegisterActivity.this.finish();
    }

}
