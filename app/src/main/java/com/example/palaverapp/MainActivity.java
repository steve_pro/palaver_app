package com.example.palaverapp;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class MainActivity extends BaseActivity implements LocationListener {

    private static String FRIEND = "";
    static final String MESSAGES_FILE = "messages.txt";
    public static final String TAG = MainActivity.class.getSimpleName();
    static HashMap<String, String> msg_map = new HashMap<>();

    SharedPreference sharedpref;
    Activity activity;
    TextView friend_name;
    ListView convo;
    EditText msgToSend;
    ImageButton sendBtn, locationBtn;
    ImageView friend_photo;
    private static String usernameStr;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<Message> messageArrayList;
    private Toolbar mToolbar;
    private HttpHandler httpHandler;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationRequest mLocationRequest;
    private LocationCallback mLocationCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        activity = this;
        sharedpref = new SharedPreference();
        httpHandler = new HttpHandler(this);
        msgToSend = (EditText) findViewById(R.id.et_myMessage);
        sendBtn = (ImageButton) findViewById(R.id.btn_send);
        locationBtn = (ImageButton) findViewById(R.id.btn_location);
        friend_name = (TextView) findViewById(R.id.tv_friendName);
        friend_photo = (ImageView) findViewById(R.id.iv_friendPhoto);
        mToolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//for the back button. see activity in manifest

        recyclerView = (RecyclerView) findViewById(R.id.rv_messageList);
        mLayoutManager = new LinearLayoutManager(this);
        ((LinearLayoutManager) mLayoutManager).setStackFromEnd(true);
        messageArrayList = new ArrayList<Message>();

        Intent intent = getIntent();//check the origin of intent
        String str = intent.getStringExtra("friend_key");
        setFriendName(str);
        friend_name.setText(str);
        if (this.checkConnectivity()) {
            if (getAllMessages(MainActivity.FRIEND).size() > 0) {
                String intentSourceString = intent.getStringExtra("fragment");
                if(intentSourceString != null){
                    if (intentSourceString.equals(KontactFragment.TAG)) {
                        intent.putExtra("friend_key", str);
                        setResult(RESULT_OK, intent);
                    }
                }

                messageArrayList.addAll(getAllMessages(MainActivity.FRIEND));
            }
            loadMessagesFromFile();
        } else {
            HashMap<String, String> map = loadMessagesFromFile();
            ArrayList<Message> list = new ArrayList<Message>();
            String data = map.get(MainActivity.FRIEND);
            if (data == null) {
                //not had a chat with that friend before
            }
            if (data != null) {
                String user_name = sharedpref.getLoggedUser(this, SharedPreference.KEY_USERNAME);
                try {
                    JSONArray jsonArray = new JSONArray(data);
                    JSONObject msg_JSON;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        msg_JSON = jsonArray.getJSONObject(i);
                        String sender = msg_JSON.getString("Sender");
                        if (msg_JSON.getString("Mimetype").equals("text/plain")) {
                            String sent_msg = msg_JSON.getString("Data");
                            Message message;
                            if (sender.equals(user_name)) {
                                //user is the sender of the message
                                message = new Message(sent_msg, true);
                            } else {
                                //user is the receiver of the message
                                message = new Message(sent_msg, false);
                            }
                            list.add(message);
                        }
                        else if(msg_JSON.getString("Mimetype").equals("text/strings")){
                            String sent_msg = msg_JSON.getString("Data");
                            Message message;
                            if(sender.equals(user_name)){
                                //user is the sender of the message
                                message = new Message(sent_msg, true, true);
                            }
                            else{
                                //user is the receiver of the message
                                message = new Message(sent_msg, false, true);
                            }
                            list.add(message);
                        }
                        else {
                            // message is a file

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (list.size() > 0) {//add friend to chat fragment if already made contact to friend
                if (intent.getStringExtra("fragment").equals(KontactFragment.TAG)) {
                    intent.putExtra("friend_key", str);
                    setResult(RESULT_OK, intent);
                }
            }
            messageArrayList.addAll(list);
        }
        mAdapter = new MessageListAdapter(this, messageArrayList);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
        //recyclerView.smoothScrollToPosition(mAdapter.getItemCount()-1);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mimetype = "text/plain";
                String msgString = msgToSend.getText().toString();
                sendDataToFriend(mimetype, msgString);
            }
        });
        if (!(this.checkConnectivity())) {
            Toast.makeText(MainActivity.this, "Oops, no internet connection", Toast.LENGTH_LONG).show();
        }
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)
                .setFastestInterval(1 * 1000);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    sendLocation(location);
                }
            }
        };
        locationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.
                        checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
                Task<Location> task = fusedLocationClient.getLastLocation();
                task.addOnSuccessListener(MainActivity.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if (location != null) {
                            sendLocation(location);
                        } else {
                            if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                            Log.d(TAG, "location was null so request location update");
                            fusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                            fusedLocationClient.removeLocationUpdates(mLocationCallback);
                        }
                    }
                });
            }
        });

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.d("Location test ",Double.toString(location.getLatitude()));
        sendLocation(location);
    }

    private void sendLocation(Location location) {
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
      /*  Intent locIntent = new Intent(MainActivity.this, MapsActivity.class);
        locIntent.putExtra("latitude", latitude);
        locIntent.putExtra("longitude", longitude);
        startActivity(locIntent);*/
       String mimetype = "text/strings";
       String lat_str = Double.toString(latitude);
       String lon_str = Double.toString(longitude);
       Log.d(TAG, "location :"+lat_str+","+lon_str);
       sendDataToFriend(mimetype, lat_str+","+lon_str);
    }

    private void sendDataToFriend(String mimeType, String msgString){
        MainActivity mainActivity = (MainActivity) activity;
        if (mainActivity.checkConnectivity()) {
            if (!msgString.equals("")) {
                String username = sharedpref.getLoggedUser(activity, SharedPreference.KEY_USERNAME);
                String password = sharedpref.getLoggedUser(activity, SharedPreference.KEY_PASSWORD);
                if (!password.equals("") && !password.equals(SharedPreference.DEFAULT_STR)
                        && !username.equals("") && !username.equals(SharedPreference.DEFAULT_STR)) {

                    String url = HttpHandler.baseURLString + "/api/message/send";
                    String recipient = MainActivity.FRIEND;
                    String hashed_password = new Hashing().encode(password);
                    String jsonStr = "{\"Username\":\"" + username + "\",\"Password\":\"" + hashed_password + "\", " +
                            "\"Recipient\":\"" + recipient + "\", \"Mimetype\":\"" + mimeType + "\", \"Data\":\"" + msgString + "\"}";
                    RequestAsyncTask requestAsyncTask = new RequestAsyncTask(MainActivity.this);
                    requestAsyncTask.execute(url, jsonStr, httpHandler);
                    if(mimeType.equals("text/plain")){
                        Message myMessage = new Message(msgString, true);
                        msgToSend.setText("");
                        messageArrayList.add(myMessage);
                        recyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
                        mAdapter.notifyDataSetChanged();
                    }
                    else if(mimeType.equals("text/strings")){
                        Message myMessage = new Message(msgString, true, true);
                        messageArrayList.add(myMessage);
                        recyclerView.smoothScrollToPosition(mAdapter.getItemCount() - 1);
                        mAdapter.notifyDataSetChanged();
                    }
                    else {

                    }
                    try {
                        String response = requestAsyncTask.get();
                        if (!response.equals(null)) {
                            JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                            if (jsonObjectResponse.getInt("MsgType") == 1) {
                                Toast.makeText(MainActivity.this, "Message sent", Toast.LENGTH_LONG).show();
                                getAllMessages(recipient);

                            } else {
                                //MsgType is 0
                                Toast.makeText(MainActivity.this, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Password not found", Toast.LENGTH_LONG).show();
                    return;
                }
            } else {
                return;
            }
        } else {
            Toast.makeText(mainActivity, "Oop, no internet connection", Toast.LENGTH_LONG).show();
        }

    }

    private void setFriendName(String friend_key) {
        FRIEND = friend_key;
    }

    @Override
    //when you press the back button
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            MainActivity.this.finish();
        }
        return true;
    }

    public ArrayList<Message> getAllMessages(String recipient){
        ArrayList<Message> messages_list = new ArrayList<Message>();
        String username = sharedpref.getLoggedUser(activity, SharedPreference.KEY_USERNAME);
        String password = sharedpref.getLoggedUser(activity, SharedPreference.KEY_PASSWORD);
        if(!password.equals("") && !password.equals(SharedPreference.DEFAULT_STR)
                && !username.equals("") && !username.equals(SharedPreference.DEFAULT_STR)){
            String url = HttpHandler.baseURLString+"/api/message/get";
            String jsonStr = "{\"Username\":\""+username+"\",  \"Password\":\""+new Hashing().encode(password)+"\",  \"Recipient\":\""+recipient+"\"}";
            RequestAsyncTask requestAsyncTask = new RequestAsyncTask(MainActivity.this);
            requestAsyncTask.execute(url, jsonStr, httpHandler);
            try {
                String response = requestAsyncTask.get();
                if (!response.equals(null)) {
                    JSONObject jsonObjectResponse = httpHandler.stringToJSON(response);
                    if (jsonObjectResponse.getInt("MsgType") == 1) {
                        //messages successfully retrieved
                        FileOutputStream outputStream = null;
                        try {
                            outputStream = openFileOutput(MESSAGES_FILE, Context.MODE_PRIVATE);
           //                 outputStream.write(sb.toString().getBytes());
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        final JSONArray data = jsonObjectResponse.getJSONArray("Data");
                       if(data.length() > 0){
                           final String friend = recipient;
                           storeMessagesToFile(friend, data);
                           JSONObject msg_JSON;
                           for(int i = 0; i<data.length(); i++){
                               msg_JSON = data.getJSONObject(i);
                               String sender = msg_JSON.getString("Sender");
                               if(msg_JSON.getString("Mimetype").equals("text/plain")){
                                   String sent_msg = msg_JSON.getString("Data");
                                   Message message;
                                   if(sender.equals(username)){
                                       //user is the sender of the message
                                       message = new Message(sent_msg, true);
                                   }
                                   else{
                                       //user is the receiver of the message
                                       message = new Message(sent_msg, false);
                                   }
                                   messages_list.add(message);
                               }
                               else if(msg_JSON.getString("Mimetype").equals("text/strings")){
                                   String lat_lon = msg_JSON.getString("Data");
                                   Message message;
                                   if(sender.equals(username)){
                                       //user is the sender of the message
                                       message = new Message(lat_lon, true, true);
                                   }
                                   else{
                                       //user is the receiver of the message
                                       message = new Message(lat_lon, false, true);
                                   }
                                   messages_list.add(message);
                               }
                               else {
                                   // message is a file

                               }
                           }
                       }
                       else{
                           //users have not yet had a chat i.e. data.length==0

                       }
                    }
                    else{
                        //MsgType is 0
                        Toast.makeText(MainActivity.this, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    //ERROR... no respose from server
                    Toast.makeText(MainActivity.this, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                }
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return  messages_list;

    }

    private void storeMessagesToFile(String recipient, JSONArray data){
        boolean recipientExists = false;
        String str = msg_map.get(recipient);

        if(!TextUtils.isEmpty(str)){
            recipientExists = true;
        }
        if(recipientExists){
            //recipient exists in file already
            //update the recipient data
            File inputFile = new File(MainActivity.MESSAGES_FILE);
            File tempFile = new File("msgTempFile.txt");

            BufferedReader reader = null;
            BufferedWriter writer = null;
            try {
                reader = new BufferedReader(new FileReader(inputFile));
                writer = new BufferedWriter(new FileWriter(tempFile));
                String currentLine;
                while((currentLine = reader.readLine()) != null) {
                    if(currentLine.substring(0, str.indexOf("(")).equals(recipient)) {continue;}
                    writer.write(currentLine + System.getProperty("line.separator"));
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if(reader != null && writer!= null) {
                    try {
                        reader.close();
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            //renaming of th files
            tempFile.renameTo(inputFile);
            inputFile.delete();
        }
        StringBuilder sb = new StringBuilder();
        sb.append(recipient).append("(");
        sb.append(data.toString());
        sb.append(")").append(System.getProperty("line.separator"));
        FileOutputStream outputStream = null;
        try {
            outputStream = openFileOutput(MESSAGES_FILE, Context.MODE_PRIVATE);
            outputStream.write(sb.toString().getBytes());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
  public void Createfile(){
        File root= new File(Environment.getExternalStorageDirectory(),"Folder");
        if(!root.exists()){
            root.mkdir();
        }

        File filepath= new File (root, "messages.txt");
  }
    private HashMap<String, String> loadMessagesFromFile(){
        FileInputStream inputStream = null;

        try {
            Createfile();
            inputStream = openFileInput(MESSAGES_FILE);
            InputStreamReader reader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(reader);
            StringBuilder sb= new StringBuilder();
            String str;
            while((str = bufferedReader.readLine()) != null){
                String recipient = str.substring(0, str.indexOf("("));
                String data = str.substring(str.indexOf("(")+1, str.indexOf(")"));
                msg_map.put(recipient, data);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  msg_map;
    }

}
