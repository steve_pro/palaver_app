package com.example.palaverapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;


public class SettingFragment extends Fragment {

    Button changePasswordButton, logoutButton;
    EditText currentPassEditText, newPassEditText;
    HomeActivity homeActivity;
    public static final String TAG = SettingFragment.class.getSimpleName();

    public static SettingFragment getInstance() {
        return new SettingFragment();
    }

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settingfragment, container, false);
        changePasswordButton = (Button) view.findViewById(R.id.btn_change_password);
        logoutButton = (Button) view.findViewById(R.id.btn_logout);
        currentPassEditText = (EditText) view.findViewById(R.id.et_change_pass_old);
        newPassEditText = (EditText) view.findViewById(R.id.et_change_pass_new);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        homeActivity = (HomeActivity) getActivity();
        changePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = HttpHandler.baseURLString+"/api/user/password";
                String username = homeActivity.sharedPreference.getLoggedUser(homeActivity, SharedPreference.KEY_USERNAME);
                Hashing hashing = new Hashing();
                String hashedPassword = hashing.encode(currentPassEditText.getText().toString());
                final String newPassword = newPassEditText.getText().toString();
                String hashedNewpassword = hashing.encode(newPassword);
                if(!homeActivity.checkConnectivity()){
                    Toast.makeText(homeActivity, "Connect to Internet", Toast.LENGTH_SHORT);
                }
                else{
                    String jsonStr = "{\"Username\":\""+username+"\",\"Password\":\""+hashedPassword+"\", " +
                            "\"NewPassword\":\""+hashedNewpassword+"\"}";
                    RequestAsyncTask requestAsyncTask = new RequestAsyncTask(homeActivity);
                    requestAsyncTask.execute(url, jsonStr, homeActivity.httpHandler);
                    //Checking the result of the POST request
                    //String response = this.getServerResponse();
                    String response = null;
                    try {
                        response = requestAsyncTask.get();
                        if (!response.equals(null)) {
                            //JSONObject jsonObjectResponse = new JSONObject(response);
                            JSONObject jsonObjectResponse = homeActivity.httpHandler.stringToJSON(response);
                            if (jsonObjectResponse.getInt("MsgType") == 1) {
                                //password change successful
                                Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                                new Thread(new Runnable() {
                                    @Override
                                    public void run() {
                                        homeActivity.sharedPreference.setLoggedUser(homeActivity, SharedPreference.KEY_PASSWORD, newPassword);
                                    }
                                }).start();
                            }
                            else{
                                //MsgType is 0
                                Toast.makeText(homeActivity, jsonObjectResponse.getString("Info"), Toast.LENGTH_LONG).show();
                            }
                        }
                        else{
                            //ERROR... no respose from server
                            Toast.makeText(homeActivity, "ERROR.. no Server response", Toast.LENGTH_LONG).show();
                        }
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.sharedPreference.setLoggedUser(homeActivity, SharedPreference.KEY_USERNAME, "");
                homeActivity.sharedPreference.setLoggedUser(homeActivity, SharedPreference.KEY_PASSWORD, "");
                Intent intent = new Intent(homeActivity, com.example.palaverapp.LoginActivity.class);
                startActivity(intent);
                homeActivity.finish();
            }
        });
    }
}
