package com.example.palaverapp;
import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

//import com.example.Palaver.R;

public class Splashscreen extends AppCompatActivity {

    SharedPreference sharepref;
    Activity activity;
    private Handler mhandler;
    private Runnable runnable;
    static  boolean isLoggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        activity = this;
        sharepref = new SharedPreference();
        String loggedUser = sharepref.getLoggedUser(activity, SharedPreference.KEY_USERNAME);
        if (!loggedUser.equals("") && !loggedUser.equals(SharedPreference.DEFAULT_STR)) {
            //logged in, do not show splashscreen
            Intent intent = new Intent(Splashscreen.this, HomeActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_splaschschreen);

            runnable = new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
            };
            mhandler = new Handler();
            mhandler.postDelayed(runnable, 2000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mhandler!=null && runnable!=null)
        mhandler.removeCallbacks(runnable);
    }
}